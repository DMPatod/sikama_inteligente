package daniel.sikama_inteligente;

import android.Manifest;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button conectarBT;

    private ListView devicesList;

    private ArrayAdapter<String> devicesArrayAdapter;
    private ArrayList<String> devicesArrayAdress = new ArrayList<String>();

    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        conectarBT = (Button) findViewById(R.id.bt_conectar);

        int PERMISSIONS_MULTIPLE_REQUEST = 123;
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH}, PERMISSIONS_MULTIPLE_REQUEST);

        registerBroadcast();

        conectarBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.cancelDiscovery();
                }
                mBluetoothAdapter.startDiscovery();

                showDialog();
            }
        });

        if (!mBluetoothAdapter.isEnabled()) {
            conectarBT.setClickable(false);
            conectarBT.setBackgroundColor(Color.GRAY);

            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);
        }

        devicesArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
    }

    void showDialog() {

        if (!devicesArrayAdapter.isEmpty()) {
            devicesArrayAdapter.clear();
            devicesArrayAdress.clear();
        }

        final Dialog devicesDialog = new Dialog(this);

        devicesDialog.setTitle("Dispositivos Disponiveis:");
        devicesDialog.setContentView(R.layout.dialog_connect);

        devicesList = (ListView) devicesDialog.findViewById(R.id.devicesList);
        devicesList.setAdapter(devicesArrayAdapter);

        devicesList.setOnItemClickListener(selectDevice);

        devicesDialog.show();
    }

    private AdapterView.OnItemClickListener selectDevice = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

            Intent intent = new Intent(getApplicationContext(), BluetoothService.class);
            intent.putExtra(Constants.MESSAGE_DEVICE_ADRESS, devicesArrayAdress.get(position));

            startService(intent);
//            finish();
        }
    };

    private boolean comunication_State = false;

    boolean test = false;

    private void registerBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(Constants.COMUNICATION_CONECTED);
        filter.addAction(Constants.ACTIVITY_READY);
        this.registerReceiver(mReceiver, filter);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {

                //mudar
                case Constants.COMUNICATION_CONECTED:
                    comunication_State = true;

                    Toast.makeText(context, "Dispositivo Conectado", Toast.LENGTH_SHORT).show();
                    break;

                //mudar
                case Constants.ACTIVITY_READY:

                    unregisterReceiver(mReceiver);
                    if (!test) {
                        test = true;
                        String[] valueString = intent.getStringArrayExtra(Constants.STRINGVALUEARRAY);
                        intent = new Intent(getApplicationContext(), Main2Activity.class);
                        intent.putExtra(Constants.STRINGVALUEARRAY, valueString);
                        startActivity(intent);
                    }
                    break;

                case BluetoothDevice.ACTION_FOUND:
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    devicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                    devicesArrayAdress.add(device.getAddress());
                    break;

                case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                    Toast.makeText(getApplicationContext(), "Busca Por Dispositivos Iniciada", Toast.LENGTH_SHORT).show();
                    break;

                case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    Toast.makeText(getApplicationContext(), "Busca Por Dispositivos Encerrada", Toast.LENGTH_SHORT).show();
                    break;

                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    if (mBluetoothAdapter.isEnabled()) {
                        conectarBT.setClickable(true);
                        conectarBT.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.colorPrimaryDark));
                    }
                    break;
            }

        }
    };
}
