package daniel.sikama_inteligente;

/**
 * Created by danie on 13-May-17.
 */

public class ComandItem {

    private String msg;
    private int value;

    public ComandItem(String msg, int value){
        this.msg = msg;
        this.value = value;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void add1(){
        value++;
    }

    public void minor1(){
        value--;
    }
}
