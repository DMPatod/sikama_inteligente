package daniel.sikama_inteligente;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * Created by danie on 13-May-17.
 */

public class BluetoothService extends Service {

    private static final String myUUID = "00001101-0000-1000-8000-00805f9b34fb";

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket mBluetoothSocket;
    private BluetoothDevice mBluetoothDevice;
    private String deviceAdress;

    private OutputStream outputStream;
    private InputStream inputStream;

    private Thread serviceThread;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Service", "onStartCommand");

        registerBroadcast();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        deviceAdress = (String) intent.getSerializableExtra(Constants.MESSAGE_DEVICE_ADRESS);

        try{
            startConnection(deviceAdress);
        } catch (IOException e) {
            Log.e("Service", "Unable To StartConnection");
            e.printStackTrace();

            //Não Pude Testar
            Intent intent1 = new Intent(Constants.SERVICE_ERROR);
            sendBroadcast(intent1);

            this.stopSelf();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void startConnection(String remoteDevice) throws IOException {
        Log.d("Service","startConnection");

        mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(remoteDevice);
        if (mBluetoothDevice == null) {
            Log.d("Service", "trying fallback");
            fallback();
        }

        startDeviceConnection();
    }

    private void startDeviceConnection() throws IOException {
        Log.d("Service", "startDeviceConnection");

        serviceThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("ServiceThread", "startDeviceConnection");

                mBluetoothAdapter.cancelDiscovery();

                try {
                    Method m = mBluetoothDevice.getClass().getMethod("createRfcommSocket", int.class);
                    mBluetoothSocket = (BluetoothSocket) m.invoke(mBluetoothDevice, 1);
                    mBluetoothSocket.connect();
                } catch (Exception e) {
                    Log.e("Service", "Trying Fallback");
                    e.printStackTrace();
                    fallback();
                }

                Log.d("ServiceThread", "Socket Created");

                try{
                    outputStream = mBluetoothSocket.getOutputStream();
                    inputStream = mBluetoothSocket.getInputStream();
                    Log.d("ServiceThread", "Successful Created in/outputStream");

                } catch (IOException e) {
                    Log.e("ServiceThread", "Unable To Create in/outputStream");
                    e.printStackTrace();

                    //Não Pude Testar
                    notifyServiceError();
                }

                //DEU CERTO
                readInputStream();
            }
        });
        serviceThread.start();
    }

    public void readInputStream(){
        Log.d("READ", "Begin");

        final int BUFFER_SIZE = 1024;
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytes = 0;
        int begin = 0;

        while (mBluetoothSocket != null){
            try{
                bytes += inputStream.read(buffer, bytes, buffer.length - bytes);
                for(int i = begin; i < bytes; i++){
                    if(buffer[i] == "\n".getBytes()[0]){

                        Intent intent = new Intent(Constants.MESSAGE_STATE_CHANGE);

                        intent.putExtra(Constants.MESSAGE_BEGIN, begin);
                        intent.putExtra(Constants.MESSAGE_I,i);
                        intent.putExtra(Constants.MESSAGE_BUFFER, buffer);

                        sendBroadcast(intent);

                        begin = i + 1;
                        if(i == bytes - 1){
                            bytes = 0;
                            begin = 0;
                        }
                    }
                }
            } catch (IOException e) {
                Log.e("readInputStream","Unable To Comunicate in InputStream");
                e.printStackTrace();

                //Não Pude Testar
                this.notifyServiceError();
                this.stopSelf();

                break;
            }
        }

    }

    public void fallback(){

        try {

            BluetoothDevice btDevice = mBluetoothAdapter.getRemoteDevice(deviceAdress);
            mBluetoothSocket = btDevice.createRfcommSocketToServiceRecord(UUID.fromString(myUUID));

            if (mBluetoothSocket != null){
                mBluetoothSocket.connect();
            }

        } catch (IOException e) {
            Log.e("fallback", "Unable To Create Socket");
            e.printStackTrace();

            //Não Pude Testar
            this.notifyServiceError();
            this.stopSelf();
        }
    }

    public void writeOutputStream(byte[] buffer) {
        try {
            outputStream.write(buffer);
        } catch (IOException e) {
            Log.e("writeOutputStream", "Exception during write", e);
            e.printStackTrace();

            //Não Pude Testar
            this.notifyServiceError();
            this.stopSelf();
        }
    }


    //Mudar
    @Override
    public void onDestroy() {

        serviceThread = null;

        mBluetoothSocket = null;

        outputStream = null;
        inputStream = null;

        mBluetoothDevice = null;

        mBluetoothAdapter = null;

        unregisterReceiver(mReceiver);

        super.onDestroy();
    }

    private void notifyServiceError(){
        Intent intent = new Intent(Constants.SERVICE_ERROR);
        sendBroadcast(intent);
    }

    private void registerBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.MESSAGE_WRITED);
        filter.addAction(Constants.MESSAGE_STATE_CHANGE);
        this.registerReceiver(mReceiver,filter);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action){
                case Constants.MESSAGE_WRITED:
                    String writedMessage = (String) intent.getSerializableExtra(Constants.MESSAGE_WRITED_STRING);
                    byte[] bytesMessage = writedMessage.getBytes();

                    writeOutputStream(bytesMessage);
                    break;

                case Constants.MESSAGE_STATE_CHANGE:
                    byte[] writeBuf = (byte[]) intent.getSerializableExtra(Constants.MESSAGE_BUFFER);
                    int begin = (int) intent.getSerializableExtra(Constants.MESSAGE_BEGIN);
                    int end = (int) intent.getSerializableExtra(Constants.MESSAGE_I);

                    String writeMessage = new String(writeBuf);
                    writeMessage = writeMessage.substring(begin, end);

                    String[] valueString = writeMessage.split(",");

                    if (valueString[0].equals("*")){

                        intent = new Intent(Constants.ACTIVITY_READY);
                        intent.putExtra(Constants.STRINGVALUEARRAY, valueString);
                        sendBroadcast(intent);
                    }
            }
        }
    };
}
