package daniel.sikama_inteligente;

import java.util.ArrayList;

/**
 * Created by danie on 14-May-17.
 */

public class ComandSingleton {

    private ArrayList<ComandItem> commandArray;

    private float[] deviceValues = new float[4];

    public static ComandSingleton getOurInstance() {
        return ourInstance;
    }

    private static final ComandSingleton ourInstance = new ComandSingleton();

    public static ComandSingleton getInstance() {
        return ourInstance;
    }

    private ComandSingleton() {
    }

    public ArrayList<ComandItem> getCommandArray() {
        return commandArray;
    }

    public void setCommandArray(ArrayList<ComandItem> commandArray) {
        this.commandArray = commandArray;
    }

    public float[] getDeviceValues() {
        return deviceValues;
    }

    public void setDeviceValues(float[] deviceValues) {
        this.deviceValues = deviceValues;
    }
}
