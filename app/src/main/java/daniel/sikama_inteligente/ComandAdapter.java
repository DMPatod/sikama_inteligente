package daniel.sikama_inteligente;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by danie on 13-May-17.
 */

public class ComandAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<ComandItem> items;

    public ComandAdapter(Context context, ArrayList<ComandItem> items){
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null){
            view = LayoutInflater.from(context).inflate(R.layout.list_comand,viewGroup,false);
        }

        final ComandItem currentItem = (ComandItem) getItem(i);

        TextView msg = (TextView) view.findViewById(R.id.msg);
        TextView value = (TextView) view.findViewById(R.id.value);
        ImageButton minor = (ImageButton) view.findViewById(R.id.minor);
        ImageButton add = (ImageButton) view.findViewById(R.id.add);

        msg.setText(currentItem.getMsg());
        value.setText(currentItem.getValue()+"");

        minor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentItem.minor1();
                Intent intent = new Intent(Constants.NOTIFIYDATACHANGE);
                context.sendBroadcast(intent);
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentItem.add1();
                Intent intent = new Intent(Constants.NOTIFIYDATACHANGE);
                context.sendBroadcast(intent);
            }
        });

        return view;
    }
}
