package daniel.sikama_inteligente;

/**
 * Created by danie on 13-May-17.
 */

public interface Constants {
    public static final String MESSAGE_STATE_CHANGE = "MESSAGE_STATE_CHANGE";
    public static final String MESSAGE_BUFFER = "MESSAGE_BUFFER";
    public static final String MESSAGE_BEGIN = "MESSAGE_BEGIN";
    public static final String MESSAGE_I = "MESSAGE_I";

    public static final String MESSAGE_WRITED = "MESSAGE_WRITED";
    public static final String MESSAGE_WRITED_STRING = "MESSAGE_WRITED_STRING";

    public static final String NEW_MESSAGE_NOTIFIE = "MESSAGE_NOTIFIE";

    public static final String ACTIVITY_READY = "ACTIVITY_READY";
    public static final String STRINGVALUEARRAY = "STRINGVALUEARRAY";

    public static final String COMUNICATION_CONECTED = "COMUNICATION_CONECTED";

    public static final String MESSAGE_DEVICE_ADRESS = "MESSAGE_DEVICE_ADRESS";

    public static final String NOTIFIYDATACHANGE = "NOTIFIYDATACHANGE";


    public static final String SERVICE_ERROR = "SERVICE_ERROR";
}
