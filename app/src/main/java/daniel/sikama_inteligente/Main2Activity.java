package daniel.sikama_inteligente;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    ListView comandList;
    ComandAdapter comandAdapter;
    ArrayList<ComandItem> comandItemArrayList;

    IntentFilter filter;

    Button des;
    Button send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        filter = new IntentFilter(Constants.NOTIFIYDATACHANGE);
        this.registerReceiver(mReceiver, filter);

        des = (Button) findViewById(R.id.des);
        des.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), BluetoothService.class);
                stopService(intent);

                killApp();
            }
        });

        send = (Button) findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int[] value = new int[4];
                ArrayList<ComandItem> temp = ComandSingleton.getInstance().getCommandArray();
                for (int i = 0; i < temp.size(); i++){
                    value[i] = temp.get(i).getValue();
                }
                String msg = "*,E,"+value[0]+","+value[1]+","+value[2]+","+value[3]+",#";

                Intent intent = new Intent(Constants.MESSAGE_WRITED);
                intent.putExtra(Constants.MESSAGE_WRITED_STRING, msg);
                sendBroadcast(intent);
            }
        });

        comandList = (ListView) findViewById(R.id.comandList);

        comandItemArrayList = new ArrayList<>();

        String[] valueArray = (String[]) getIntent().getSerializableExtra(Constants.STRINGVALUEARRAY);
        for (int i = 1; i + 1 < valueArray.length; i = i + 2) {
            String msg = valueArray[i];
            int value = Integer.parseInt(valueArray[i + 1]);
            comandItemArrayList.add(new ComandItem(msg, value));
        }

        ComandSingleton.getInstance().setCommandArray(comandItemArrayList);

        comandAdapter = new ComandAdapter(getApplicationContext(), ComandSingleton.getInstance().getCommandArray());
        comandList.setAdapter(comandAdapter);
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {
                case Constants.NOTIFIYDATACHANGE: {
                    comandAdapter.notifyDataSetChanged();
                }
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main2menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.reset:

                String msg = "*,R,4,#";
                Intent intent = new Intent(Constants.MESSAGE_WRITED);
                intent.putExtra(Constants.MESSAGE_WRITED_STRING, msg);
                sendBroadcast(intent);

                intent = new Intent(getApplicationContext(), BluetoothService.class);
                stopService(intent);

                killApp();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void killApp() {
        Intent mStartActivity = new Intent(getApplicationContext(), MainActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }
}
